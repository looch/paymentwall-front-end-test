import { combineReducers } from 'redux'
import {default as shop} from './shopReducer'

const rootReducer = combineReducers({
    shop
});

export default rootReducer 