import {sleep, updateQuantity} from "./helpers/helpers";

const PRODUCTS_ENDPOINT = 'https://api.myjson.com/bins/baedn'
const LOAD_PRODUCTS = 'LOAD_PRODUCTS'
const LOAD_PRODUCTS_REQUESTED = 'LOAD_PRODUCTS_REQUESTED'
const LOAD_PRODUCT_SUCCESS = 'LOAD_PRODUCT_SUCCESS'
const LOAD_PRODUCTS_FAIL = 'LOAD_PRODUCTS_FAIL'
const PROCESS_ORDER_REQUESTED = 'PROCESS_ORDER_REQUESTED'
const PROCESS_ORDER_SUCCESS = 'PROCESS_ORDER_SUCCESS'
const ADD_TO_CART = 'ADD_TO_CART'
const CHANGE_QUANTITY = 'CHANGE_QUANTITY'
const REMOVE_ITEM = 'REMOVE_ITEM'
const CLEAR_CART = 'CLEAR_CART'

const initialState = {
    loading: false,
    error: false,
    products: null,
    cart: [],
    processing: false,
    isCompleted: false
}

export default function reducer(state = initialState,
                                action = {}) {
    switch (action.type) {

        case LOAD_PRODUCTS_REQUESTED: {
            return {
                ...state,
                loading: true
            }
        }
        case LOAD_PRODUCT_SUCCESS: {
            return {
                ...state,
                loading: false,
                products: action.payload.data
            }
        }
        case LOAD_PRODUCTS_FAIL:{
            return{
                ...state,
                loading:false,
                error:action.payload.error
            }
        }
        case ADD_TO_CART: {
            const {id} = action.payload
            const existingItem = state.cart.find(item => item.id === id);
            const cart = state.cart.filter((item) => item.id !== id);
            return {
                ...state,
                cart: !existingItem ? [...cart, action.payload] : [...cart, {
                    ...action.payload,
                    quantity: existingItem.quantity + 1
                }]
            }
        }
        case CHANGE_QUANTITY: {
            const {id, quantity} = action.payload
            return {
                ...state,
                cart: updateQuantity(id, state.cart, quantity)
            }
        }
        case REMOVE_ITEM: {
            const {id} = action.payload
            const cart = state.cart.filter(item => item.id !== id);
            return {
                ...state,
                cart: [...cart]
            }
        }
        case CLEAR_CART: {
            return {
                ...state,
                cart: []
            }
        }
        case PROCESS_ORDER_REQUESTED: {
            return {
                ...state,
                processing: true
            }
        }
        case PROCESS_ORDER_SUCCESS: {
            return {
                ...state,
                processing: false,
                isCompleted: true
            }
        }
        default:
            return state
    }
}

export const loadProducts = () => {
    return dispatch => {
        dispatch(loadProductsRequested())
        fetch(PRODUCTS_ENDPOINT)
            .then(res => res.json())
            .then(data => dispatch(loadProductsSuccess(data)))
            .catch(err => dispatch(loadProductsFail(err)))
    }
}

const loadProductsRequested = () => ({
    type: LOAD_PRODUCTS_REQUESTED
})
const loadProductsSuccess = data => ({
    type: LOAD_PRODUCT_SUCCESS,
    payload: {
        data
    }
})
const loadProductsFail = error =>({
    type:LOAD_PRODUCTS_FAIL,
    payload:{
        error
    }
})

export const addToCart = item => ({
    type: ADD_TO_CART,
    payload: {
        ...item,
        quantity: 1
    }
})
export const changeQuantity = (id, quantity) => ({
    type: CHANGE_QUANTITY,
    payload: {
        id,
        quantity
    }
})

export const removeItem = id => ({
    type: REMOVE_ITEM,
    payload: {
        id
    }
})

export const clearCart = () => ({
    type: CLEAR_CART
})
export const processOrder = data => {
    return (dispatch, getState) => {
        const {cart} = getState();
        dispatch(processOrderRequested())
        sleep(2000).then(() => dispatch(processOrderSuccess()))

    }
}
export const processOrderRequested = () => ({
    type: PROCESS_ORDER_REQUESTED
})
export const processOrderSuccess = () => ({
    type: PROCESS_ORDER_SUCCESS
})


export const getProducts = state => state.shop && state.shop.products

export const getCartAmount = state => state.shop && state.shop.cart && state.shop.cart.reduce((prev, curr) => (curr.quantity && (prev + curr.quantity)), 0)

export const getCart = state => state.shop && state.shop.cart

export const getTotal = state => state.shop && state.shop.cart.map(item => item.price * item.quantity).reduce((prev, curr) => (curr && (prev + curr)), 0)

export const getLoading = state => state.shop && state.shop.loading

export const getProcessing = state => state.shop && state.shop.processing

export const getOrderStatus = state => state.shop && state.shop.isCompleted

export const getError = state => state.shop && state.shop.error