export const updateQuantity = (id, cart,quantity) => (cart.map(item => {
    if (item.id === id) {
        return ({
            ...item,
            quantity: quantity
        })
    }
    else {
        return ({
            ...item
        })
    }
}))

export const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));