let currentYear = new Date().getFullYear()
export const months = [1,2,3,4,5,6,7,8,9,10,11,12]

export const getYearsList = () => {
    let years = []
    for(let i = 0; i < 5; i++){
        years.push(currentYear++)
    }
    return years
}