let validEmail = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)
const expDate= new Date();
const today = new Date()
export const validate = values => {
    const errors = {}
    if (!values.cardNum) {
        errors.cardNum = "Please enter your card number"
    } else if (values.cardNum && values.cardNum.length !== 19) {
        errors.cardNum = "Please enter valid card number"
    }
    if (!values.expMonth) {
        errors.expMonth = "Required"
    }
    if (!values.expMonth) {
        errors.expMonth = "Required"
    }
    if (!values.expYear) {
        errors.expYear = "Required"
    }
    if(values.expYear && values.expMonth){
        expDate.setFullYear(parseInt(values.expYear),parseInt(values.expMonth),1)
        if(expDate<today){
            errors.expMonth = "Expired"
        }
    }
    if (!values.cvvCode) {
        errors.cvvCode = "Please enter your cvv code"
    } else if (values.cvvCode.length < 3) {
        errors.cvvCode = "Please enter valid cvv code"
    }
    if (!values.email && values.storeOption) {
        errors.email = "Please enter your email"
    } else if (values.storeOption && !validEmail.test(values.email)) {
        errors.email = "Please enter valid email"
    }

    console.log(errors)
    return errors
}