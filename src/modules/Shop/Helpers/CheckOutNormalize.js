const deleteNonNumeric = value => value.replace(/[^\d]/g, '')
export const normalizeCardNum = (value, previousValue) => {
    if (!value) {
        return value
    }
    const onlyNums = deleteNonNumeric(value)
    if (!previousValue || value.length > previousValue.length) {

        if (onlyNums.length === 4) {
            return onlyNums + ' '
        }

        if (onlyNums.length === 8) {
            return onlyNums.slice(0, 4) + ' ' + onlyNums.slice(4) + ' ';
        }
    }

    if (onlyNums.length <= 4) {
        return onlyNums;
    }

    if (onlyNums.length <= 8) {
        return onlyNums.slice(0, 4) + ' ' + onlyNums.slice(4);
    }

    if (onlyNums.length <= 12) {
        return onlyNums.slice(0, 4) + ' ' + onlyNums.slice(4, 8) + ' ' + onlyNums.slice(8, 12);
    }

    return onlyNums.slice(0, 4) + ' ' + onlyNums.slice(4, 8) + ' ' + onlyNums.slice(8, 12) + ' ' + onlyNums.slice(12, 16)
};

export const CVVNormalize = value => {
    if (!value) {
        return value
    }
    const onlyNums = deleteNonNumeric(value)
    return onlyNums.slice(0, 3);
}
