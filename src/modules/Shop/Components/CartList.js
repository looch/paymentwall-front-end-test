import React from 'react'
import CartItem from "./CartItem";
import styled from 'styled-components'


const CartList = ({amount, cartList, changeQuantity, removeItem,totalPrice,openCheckOut}) => (
    <Wrapper>
        {!amount
            ? <EmptyWarning>
                <Line>Your cart is empty.</Line>
            </EmptyWarning>
            : <div>

                <Title>Cart</Title>
                <Amount>{amount} items</Amount>
                {cartList.map(item => <CartItem {...item}
                                                changeQuantity={changeQuantity}
                                                removeItem={removeItem}
                                                key={item.id}

                />)}

                <Total>
                    <Row>
                        <div>Total</div>
                        <TotalPrice>${totalPrice}</TotalPrice>
                    </Row>
                </Total>
                 <CheckOutBtn onClick={openCheckOut}>Continue to Checkout</CheckOutBtn>
            </div>
        }
    </Wrapper>
)
export default CartList

const CheckOutBtn = styled.button`
    padding: 10px 0;
    background: #323333;
    border-radius: 4px;
    font-size: 16px;
    color: white;
    text-align: center;
    width: 100%;
    border: 0;
    cursor: pointer;
`

const Row = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 16px 0px;
   
`
const Total = styled.div`
   font-size:22px;
`
const TotalPrice = styled.strong`
   
`
const Wrapper = styled.div`

`
const Title = styled.div`
    font-size: 16px;
    margin-bottom: 10px;
`
const Amount = styled.div`
    font-size: 14px;
`
const Line = styled.p `

`
const EmptyWarning = styled.div`
    text-align: center;
`