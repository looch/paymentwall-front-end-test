import React from 'react'
import {Field} from 'react-final-form'
import styled from 'styled-components'
import {CVVNormalize, normalizeCardNum} from "../Helpers/CheckOutNormalize";
import {Loader} from "./Loader";
import {months, getYearsList} from "../Helpers/Constants";

const years = getYearsList()

const renderField = ({label, input, meta, id, required, ...rest}) => (
    <FieldWrapper>
        <Label htmlFor={id}>{label}</Label>
        <Input error={true} hasError={meta.touched && !!meta.error}><input {...input} id={id} {...rest} /></Input>
        <Error>{meta.touched && meta.error && <span>{meta.error}</span>}</Error>
    </FieldWrapper>
)


const CheckOutForm = ({handleSubmit, reset, submitting, pristine, values, processing}) => (
    <form onSubmit={handleSubmit}>

        <Field
            name="cardNum"
            id="cardNum"
            type="text"
            label="Credit Card Number"
            placeholder="Card Number"
            render={renderField}
            parse={normalizeCardNum}
        />
        <ExpDateLabel>Expiration Date</ExpDateLabel>
        <ExpDate>
            <Field
                name="expMonth"
                render={({input, meta}) => <div>
                    <select {...input}>
                        <option value="">Month</option>
                        {months.map(m => <option key={m} value={m}>{m}</option>)}
                    </select>
                    <Error>{meta.touched && meta.error && <span>{meta.error}</span>}</Error>
                </div>}
                id="expMonth"
            />

         /
            <Field
                name="expYear"
                id="expYear"
                render={({input, meta}) => <div><select {...input}>
                    <option value="" >Year</option>
                    {years.map(y => <option key={y} value={y}>{y}</option>)}
                </select>
                    <Error>{meta.touched && meta.error && <span>{meta.error}</span>}</Error>
                </div>}
            />
        </ExpDate>
        <Field
            name="cvvCode"
            id="cvvCode"
            type="text"
            label="CVV code"
            placeholder="CVV code"
            render={renderField}
            parse={CVVNormalize}
        />
        <Field
            name="storeOption"
            id="storeOption"
            render={({input, id}) => (<FieldWrapper>
                <label htmlFor={id}>
                    <input type="checkbox" {...input} id={id}/>
                    Store the card
                </label>
            </FieldWrapper>)}
        />
        {values.storeOption && <Field
            name="email"
            label="Email"
            placeholder="Enter your email"
            render={renderField}
        />}
        <Submit type="submit" disabled={pristine || processing}>
            {processing ? <Loader width={5} height={5}/> : 'submit'}
        </Submit>
    </form>
)
export default CheckOutForm

const Submit = styled.button`
    padding:0 20px;
    min-height: 37px;
    background-color: #323333;
    color: white;
    border:none;
    border-radius: 5px;
    cursor: pointer;
    &:disabled{
      background-color:#6f7171;
      cursor: not-allowed;
    }
`


const FieldWrapper = styled.div`
  width:95%;
   
`

const Input = styled.div`
    input {
        padding: 12px 16px;
        background: #fff;
        border: 1px solid #d7d9d9;
        border-radius: 4px;
        width: 100%;
        
        ${props => props.hasError && `
            color: indianred;
            border: 1px solid indianred;
        `}
    }
`
const Error = styled.div`
    color: indianred;
    margin: 0 0 10.5px;
`

const Label = styled.label`
    font-size: 14px;
    color: #575959;
    letter-spacing: 0;
    margin-top: 32px;
    margin-bottom: 16px;  
`
const ExpDate = styled.div`
  display:flex;
`
const ExpDateLabel = styled.span`
   font-size: 14px;
    color: #575959;
`
