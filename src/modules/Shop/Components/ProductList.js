import React from 'react'
import StoreItem from "./StoreItem";
import styled from 'styled-components'

const ProductList = ({products,addToCart}) =>(
    <StyledList>
        {products && products.map(item => <StoreItem {...item} key={item.id} addToCart={addToCart}/>)}
    </StyledList>

)
export default ProductList

const StyledList = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
`