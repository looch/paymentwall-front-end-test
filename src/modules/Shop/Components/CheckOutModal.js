import React from 'react'
import styled from 'styled-components'
import {Form} from "react-final-form"
import CheckOutForm from './CheckOutForm'
import {validate} from "../Helpers/CheckOutValidate";

const CheckOutModal = ({closeCheckOut, onSubmit, isCompleted, processing}) => (
    <ModalWrapper>
        <Modal>
            <CloseButton type="button" onClick={closeCheckOut}>X</CloseButton>
            {isCompleted
                ? <Message>Payment is completed</Message>
                : <Form
                    render={CheckOutForm}
                    onSubmit={onSubmit}
                    initialValues={{storeOption: false}}
                    validate={validate}
                    subscription={{submitting: true, pristine: true, values: true}}
                    processing={processing}

                />
            }

        </Modal>
    </ModalWrapper>
)
export default CheckOutModal

const ModalWrapper = styled.div`
  display: flex;
  align-items: center;
  position: fixed;
  top: 0;
  left: 0;
  width:100vw;
  height:100vh;
  background:rgba(0,0,0,0.7)
`
const Modal = styled.div`
    position:relative;
    width: 600px;
    box-sizing: border-box;
    margin: 0 auto;
    background: white;
    display: block;
    padding: 5px 10px;
    min-height: 280px;
`
const CloseButton = styled.button`
    position: absolute;
    right: 0;
    font-size: 20px;
    border: none;
    cursor: pointer;
`
const Message = styled.span`
    position:absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    font-size: 24px;
    color: #4CAF50;
`