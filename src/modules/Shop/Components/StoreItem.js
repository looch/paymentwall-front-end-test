import React, {Component} from 'react'
import styled from 'styled-components'


export default class StoreItem extends Component {
    handleAdd = () => {
        const {title, description, price, id, img} = this.props
        this.props.addToCart({title, description, price, id, img})
    }

    render() {
        const {title, description, price, img} = this.props
        return (
            <ProductWrapper>
                <ProductCover src={img}/>
                <Description>
                    <Title>{title}</Title>
                    <p>{description}</p>
                    <PriceWrapper>
                        <Price>${price}</Price>
                    </PriceWrapper>
                    <AddButton onClick={this.handleAdd}>Add to cart</AddButton>
                </Description>
            </ProductWrapper>
        )
    }
}
const Title = styled.h3`
  
`

const AddButton = styled.button`
  display:block;
  width: 100%;
  font-size: 16px;
  padding:5px 6px;
  cursor: pointer;
  background-color: #323333;
  color:white;
`
const ProductWrapper = styled.div`
   flex: 0 0 30%;
   padding:5px;
   margin-bottom: 10px;
   border: 1px solid #eee;
`
const Description = styled.div`

`
const Price = styled.strong`
  

`
const PriceWrapper = styled.div`
 
`
const ProductCover = styled.img`
  width:100%;
`