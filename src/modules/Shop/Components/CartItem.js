import React, {Component} from "react";
import styled from "styled-components";


export default class CartItem extends Component {
    handleChange = amount => {
        const {id, quantity} = this.props;
        if (quantity + amount) {
            this.props.changeQuantity(id, quantity + amount);
        } else {
            this.handleRemove();
        }
    };
    handleRemove = () => {
        const {id} = this.props;
        this.props.removeItem(id);
    };

    render() {
        const {title, img, quantity, price} = this.props;
        return (
            <Wrapper>
                <Image>
                    <img src={img} alt=""/>
                </Image>
                <Info>
                    <Row>
                        <Name>
                            {title}
                        </Name>
                        <X onClick={this.handleRemove}>X</X>
                    </Row>
                    <Separator/>
                    <Row>
                        <QuantityWrapper>
                            <Button onClick={() => this.handleChange(1)}>
                                +
                            </Button>
                            <Quantity>
                                {quantity}
                            </Quantity>
                            <Button onClick={() => this.handleChange(-1)}>
                               -
                            </Button>
                        </QuantityWrapper>
                        <span>
                            ${(price * quantity).toFixed(2)}
                        </span>
                    </Row>
                </Info>
            </Wrapper>
        );
    }
}

const Name = styled.div``;

const QuantityWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid black;
  border-radius: 4px;
  color: black;
`;
const Separator = styled.div`
  height: 20px;
`
const Button = styled.button`
  width: 25px;
  padding: 0;
  height: 25px;
  border: none;
  transition: 0.1s all;

  cursor: pointer;
  &:hover {
    background-color: #aaa;
    color: white;
  }
  &:focus {
    outline: none;
  }
  &:first-child {
    border-right: 1px solid #aaa;
  }
  &:last-child {
    border-left: 1px solid #aaa;
  }
`;
const Quantity = styled.span`
  width: 25px;
  height: 25px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  user-select: none;
`;
const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 1px solid;
  padding: 20px 0px;
    color: black;
`;
const Image = styled.div`
  margin-right: 20px;

  & > img {
    height: 80px;
  }
`;

const Price = styled.div`flex-basis: 10%;`;

const Info = styled.div`
  flex-grow: 1;
  flex-basis: 90%;
`;

const X = styled.div`cursor: pointer;`