
import styled,{keyframes} from 'styled-components'

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }`
export const Loader = styled.div`
    margin:0 auto;
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: ${props => props.width}px;
    height: ${props => props.height}px;
    animation: ${rotate360} 2s linear infinite;
`

