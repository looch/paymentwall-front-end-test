import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {
    loadProducts,
    getProducts,
    addToCart,
    getCartAmount,
    getCart,
    getTotal,
    changeQuantity,
    removeItem,
    clearCart,
    getLoading,
    getProcessing,
    processOrder,
    getOrderStatus, getError
} from "../../../redux/modules/shopReducer";
import ProductList from '../Components/ProductList'
import CartList from '../Components/CartList'
import styled from 'styled-components'
import CheckOutModal from '../Components/CheckOutModal'
import {Loader} from "../Components/Loader";

class StoreContainer extends Component {
    state = {
        showCheckOut: false
    }
    openCheckOut = () => this.setState({showCheckOut: true})
    closeCheckOut = () => this.setState({showCheckOut: false})

    componentDidMount = () => {
        this.loadProducts()
    }
    loadProducts = () => this.props.loadProducts()

    addToCart = item => this.props.addToCart(item)

    changeQuantity = (id, quantity) => this.props.changeQuantity(id, quantity)

    removeItem = id => this.props.removeItem(id)

    clearCart = () => this.props.clearCart()

    handleOrder = data => this.props.processOrder(data)

    render() {
        const {products, amount, cartList, totalPrice,loading,processing,isCompleted,error} = this.props
        const {showCheckOut} = this.state
        if(error){
            return(
                <div>Server error</div>
            )
        }
        return (
            <StoreWrapper>
                {loading
                    ? <Loader
                        width={120}
                        height={120}
                    />
                    : <ProductList
                       products={products}
                       addToCart={this.addToCart}
                /> }

                <List>
                    <CartList
                        amount={amount}
                        cartList={cartList}
                        changeQuantity={this.changeQuantity}
                        removeItem={this.removeItem}
                        clearCart={this.clearCart}
                        totalPrice={totalPrice}
                        openCheckOut={this.openCheckOut}
                    />
                </List>
                {showCheckOut && <CheckOutModal
                    closeCheckOut={this.closeCheckOut}
                    onSubmit={this.handleOrder}
                    processing={processing}
                    isCompleted={isCompleted}
                />}

            </StoreWrapper>
        )
    }
}

export default connect(
    state => ({
        products: getProducts(state),
        amount: getCartAmount(state),
        cartList: getCart(state),
        totalPrice: getTotal(state),
        loading:getLoading(state),
        processing:getProcessing(state),
        isCompleted:getOrderStatus(state),
        error:getError(state)
    }),
    dispatch => bindActionCreators({
        addToCart,
        changeQuantity,
        removeItem,
        clearCart,
        loadProducts,
        processOrder
    }, dispatch)
)(StoreContainer)

const StoreWrapper = styled.div`
  max-width: 900px;
  margin: 0 auto;
`

const List = styled.div`
    margin-top: 80px;
    margin-bottom: 40px;
`